SBT + OpenJDK 8 on Alpine Linux
===============================

This repository contains an Alpine Dockerized openjdk-8 + sbt, published to the public Docker Hub via automated build mechanism.

Usage
-----

You have two options for working with this image:
1 - Get the image from Docker Hub: `docker pull iflavoursbv/sbt-openjdk-8-alpine`
2 - Directly build the image: `docker build -t iflavoursbv/sbt-openjdk-8-alpine https://bitbucket.org/iflavours/sbt-openjdk-8-alpine/raw/master/Dockerfile`

### General usage
Please note this image is configured with a workdir `/code`, so to build your project you have to mount a volume for your sources.

```bash
$ docker run --rm -v $(pwd):/code iflavoursbv/sbt-openjdk-8-alpine:latest 'your-commands-here'
```

### You can run the default sbt command simply:
```bash
$ docker run --rm iflavoursbv/sbt-openjdk-8-alpine:latest sbt sbt-version
```

### This image can be configured to chache IVY artifacts:

```bash
$ docker run --rm -v $(pwd):/code -v "$HOME/.ivy2":/root/.ivy2 iflavoursbv/sbt-openjdk-8-alpine:latest sbt clean compile
```

Configuration
-------------
This docker image is based on the following stack:
- OS: Alpine Linux
- Java: OpenJDK 8
- SBT: latest=0.13.15 (see [tag list](https://registry.hub.docker.com/u/iflavoursbv/sbt-openjdk-8-alpine/tags/manage/) for more versions)


Dependencies
------------
- [java:openjdk-8-jdk-alpine](https://hub.docker.com/r/library/java/tags/openjdk-8-jdk-alpine/)

History
-------
* 0.1.2 - Upgrade SBT to version 0.13.15
* 0.1.1 - Upgrade SBT to version 0.13.12
* 0.1.0 - Initial version

Alternatives
------------
### hseeberger/scala-sbt/
* Location: https://hub.docker.com/r/hseeberger/scala-sbt/
* Worth considering:
 * Its effective size is 793MB (vs. 319 MB from this image)
 * It's also based on Alpine and OpenJDK 8, but it also comes with Scala installed

### 1science/sbt/
* Location: https://hub.docker.com/r/1science/sbt/
* Worth considering:
  * It comes with Oracle JRE (hence you might want to check the Oracle's license).
  * It does not have a standard license.

License
-------

[Licensed under MIT License](https://bitbucket.org/iflavours/sbt-openjdk-8-alpine/raw/master/LICENSE)

